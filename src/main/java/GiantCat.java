import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GiantCat {
    
    public static Random randomizer = new Random();
    public static  List<String> list = new ArrayList<>();
    public static List<String>  badmoves = new ArrayList<>();

    public static void main(String[] args) throws Exception {
        list.add("5");
        list.add("7");
        list.add("√");
        
        //Montecarlo
        for(int x = 1; x < 100000000;x++) {
            BrokenConsole bc = new BrokenConsole();
            if(playtowin(bc)) {
                bc.print();
                System.out.println(x+ "trys");
                break;
            } else {
                //System.out.println("x "+x);
                //bc.print();
            }
        }
        System.out.println("Fail!");
    }

    private static boolean playtowin(BrokenConsole bc) throws Exception {
        //make move
        //if won return true
        int strikes = 0;
        String moves = "";
        
        while(bc.hasBlownUp() == false && bc.hasWon() == false) {
            String random;
            strikes = 0;
            do {
                random = list.get(randomizer.nextInt(list.size()));
                strikes++;
            } while(badmoves.contains(moves+random) && strikes < 4);
            bc.keyPress(random);
            moves = moves+random;
        }
        
        if(bc.hasBlownUp()) {
            badmoves.add(moves);
            //System.out.println(moves);
        }
        return bc.hasWon();
    }

}
