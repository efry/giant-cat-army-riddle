import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GiantCatTree {
    
    public static Random randomizer = new Random();
    //public static  List<String> list = new ArrayList<>();
    public static List<String>  badmoves = new ArrayList<>();
    public static int itterations = 0;

    public static void main(String[] args) throws Exception {
       /* list.add("5");
        list.add("7");
        list.add("√");
        */
        //Tree
            BrokenConsole bc = new BrokenConsole();
            if(playtowin(bc)) {
                bc.print();
                System.out.println("Wooo hoo! Done in "+itterations);
                return;
        }
            
        System.out.println("Fail!");
    }

    private static boolean playtowin(BrokenConsole bc) throws Exception {
        itterations++;
        
        bc.keyPress("√");
        if(bc.hasWon()) return true; 
        if(!bc.hasBlownUp() && playtowin(bc)) return true;
        
        bc.removeLastKey();
        
        //System.out.println("Moving to 7");

        bc.keyPress("5");
        if(bc.hasWon()) return true; 
        if(!bc.hasBlownUp() && playtowin(bc)) return true;
        
        bc.removeLastKey();

        //System.out.println("Moving to √");

        bc.keyPress("7");
        if(bc.hasWon()) return true; 
        if(!bc.hasBlownUp() && playtowin(bc)) return true;
        
        bc.removeLastKey();

        return false; //no winners here
    }
}
