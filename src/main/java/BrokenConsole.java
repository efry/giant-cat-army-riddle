import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BrokenConsole {

    private List<Operation> operations = new ArrayList<>();
    private List<Double> totals = new ArrayList<>();
    private boolean hasBlownUp;
    private String message;

    public void keyPress(String key) throws Exception {
        if (key == "5" || key =="7" || key =="√") {
            operations.add(new Operation(key));
            this.calculateTotals();
        } else {
            throw new Exception("Invaid Key!");
        }
    }

    private void calculateTotals() {
        this.hasBlownUp = false;
        this.message = "";
        totals.clear();
        totals.add(0.0);
        
        double total = 0.0D;
        
        for( Operation op : operations) {
        //apply operation
            if(op.key == "5") total +=5;
            if(op.key == "7") total +=7;
            if(op.key == "√") total = Math.sqrt(total);
        // save total
            totals.add(total);
        // check rules
            applyRules();
        }
    }

    private void applyRules() {
        checkAllTotalsAreWhole();
        checkNoTotalBiggerThan60();
        checkNoRepeatedTotals();
    }

    private void checkNoRepeatedTotals() {
        List<Double> x = new ArrayList<>();

        for(Double total : this.totals) {
            if(x.contains(total)) {
                this.hasBlownUp = true;
                this.message = "You have a repeating total with " + total;
                return;
            }
            x.add(total);
        }
        
    }

    private void checkNoTotalBiggerThan60() {
        Optional<Double> x = totals.stream().filter(t -> (t > 60)).findFirst();
        if(x.isPresent()) {
            this.hasBlownUp = true;
            this.message = "You have a total bigger than 60! BANG!";
        }
    }

    private void checkAllTotalsAreWhole() {
        Optional<Double> x = totals.stream().filter(t -> (t % 1 != 0)).findFirst();
        if(x.isPresent()) {
            this.hasBlownUp = true;
            this.message = "You have a total that is not whole - KABOOM";
        }
    }

    public void print() {
        System.out.println("## OPERATIONS ###########################################################");

        System.out.print("# ");
        operations.forEach(o -> {
            System.out.print(o.toString() + " ");
        });
        System.out.println("#");

        System.out.println("## TOTALS ###############################################################");

        System.out.print("# ");
        totals.forEach(o -> {
            System.out.print(o.intValue() + " ");
        });
        System.out.println("#");

        System.out.println("#########################################################################");
        if(message != null) {
            System.out.println("# "+ message);
        }
    }

    public List<Operation> getOperations() {
        return this.operations;
    }

    public boolean hasBlownUp() {
        return hasBlownUp;
    }

    public int getTotal() {
        if(totals.size()==0) return 0;
        return this.totals.get(totals.size()-1).intValue();
    }

    public void addTotal(int i) {
        totals.add(new Double(i));
    }

    // has 2 10 14 in that order 
    public boolean hasWon() {
        int correct = 0;
        for(Double total : totals) {
            if(correct == 0 && total == 2) {
                correct++;
            }
            if(correct == 1 && total == 10) {
                correct++;
            }
            if(correct == 2 && total == 14) {
                return true;
            }
        }
        
        return false;
    }

    public void removeLastKey() {
        if(operations.size() > 0) {
            operations.remove(operations.size()-1);
        }
        
        this.calculateTotals();
        
    }
}
