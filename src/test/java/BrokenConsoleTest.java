import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class BrokenConsoleTest {

    private BrokenConsole bc;
    
    @Before
    public void setup() {
        this.bc = new BrokenConsole();
    }
    
    @Test
    public void testKeyPressed() throws Exception {
        
        assertEquals(bc.getOperations().size(),0);
        bc.keyPress("5");
        assertEquals(bc.getOperations().size(), 1);
        assertEquals(bc.getOperations().get(0).toString(), "+5");
    }
    
    @Test(expected=Exception.class) 
    public void testInvalidKeys() throws Exception {
        bc.keyPress("B");
    }
    
    @Test
    public void testValidKeys() throws Exception {
        bc.keyPress("5");
        bc.keyPress("7");
        bc.keyPress("√");
        
        assertEquals(bc.getOperations().size(),3);
    }
    
    @Test 
    public void testFirstMovesDoneBlowUp() throws Exception {
        bc.keyPress("5");
        assertFalse(bc.hasBlownUp());
    }
    
    @Test 
    public void testFirstMovesDoneBlowUp7() throws Exception {
        bc.keyPress("7");
        assertFalse(bc.hasBlownUp());
    }
    
    @Test 
    public void testFirstMovesDoneBlowUpSquart() throws Exception {
        bc.keyPress("√");
        bc.print();
        assertTrue(bc.hasBlownUp());  //repeating 0 total
    }
    
    
    @Test
    public void testTotalsTotalStartFromZero() {
        assertEquals(bc.getTotal(),0);
    }

    @Test
    public void testTotalsTotalTotalsFirst() throws Exception {
        bc.keyPress("5");
        assertEquals(bc.getTotal(),5);
        assertFalse(bc.hasBlownUp());
    }

    @Test 
    public void testNonWholeNumbersBlowUp() throws Exception {
        bc.keyPress("5");
        bc.keyPress("7");
        bc.keyPress("√");
        assertTrue(bc.hasBlownUp());
    }

    @Test 
    public void testWholeNumbersDontBlowUp() throws Exception {
        bc.keyPress("5");
        bc.keyPress("7");
        bc.keyPress("7");
        bc.keyPress("7");
        bc.keyPress("5");
        bc.keyPress("5");
        bc.keyPress("√");
        assertFalse(bc.hasBlownUp());
    }


    @Test 
    public void testRepeatingTotals() throws Exception {
        bc.keyPress("5");
        bc.keyPress("5");
        bc.keyPress("5");
        bc.keyPress("5");
        bc.keyPress("5");
        bc.keyPress("√");
        bc.print();
        assertTrue(bc.hasBlownUp());
    }
    
    @Test
    public void testHasWon() {
        bc.addTotal(2);
        bc.addTotal(3);
        bc.addTotal(10);
        bc.addTotal(11);
        bc.addTotal(14);
        assertTrue(bc.hasWon());
    }

    @Test 
    public void testHasNotWon() throws Exception {
        bc.addTotal(2);
        bc.addTotal(3);
        bc.addTotal(10);
        bc.addTotal(11);
        bc.addTotal(13);
        assertFalse(bc.hasWon());
    }
    
    @Test
    public void testRemoveLastKey() throws Exception {
        bc.keyPress("5");
        bc.keyPress("7");
        assertEquals(12, bc.getTotal());
        bc.removeLastKey();
        assertEquals(5, bc.getTotal());
        
        
    }
}
